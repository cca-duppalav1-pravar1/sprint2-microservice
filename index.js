const express = require('express');
const app = express();
const cors = require('cors')
app.use(cors())
app.use(express.json())
const MongoClient = require("mongodb").MongoClient;
const mongourl = "mongodb+srv://cca-duppalav1-pravar1:sprint2@cca-duppalav1-pravar1.tp7zc.mongodb.net/sprint2Database?retryWrites=true&w=majority";
const dbClient = new MongoClient(mongourl, {useNewUrlParser: true, useUnifiedTopology: true});
dbClient.connect(err=>{
    if(err) throw err;
    console.log("Connected to the MongoDB Cluster");
})
var port =  process.env.PORT || 8080;

app.use(express.static('static'))
app.use(express.urlencoded({extended:false}))
app.listen(port, () =>
    console.log(`HTTP Server with Express.js is listening on port : ${port}`))
    
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/static/cca-form.html');
})  


app.post('/signup', function(req, res){
    const {username, password, fullname, email} = req.body;
    const db = dbClient.db();
    (async () => {
        console.log("ALERT : got a sign up request!")
        try{
            var userLength = await db.collection("users").countDocuments({email:email});
            if(userLength > 0){
                res.send({"userLength" : userLength, "message" : "Email already Exists"}); 
                return           
            }
            else{
                await db.collection("users").insertOne({email:email, password : password, fullname : fullname, username : username});
                res.send({"userLength" : 1, "message": "Signed up Successfully! Please Log in now."}) 
                return           
            }
        }
        catch(err){
            console.log(err)
        }

    })();
})


app.post('/login', function(req, res){
    const {username, password, fullname, email} = req.body;
    const db = dbClient.db();

    (async () => {
        try{
            var userCount = await db.collection("users").countDocuments({email:email, password : password});
            if(userCount == 0){
                res.send({status: 1, logged:false, message : "login failed"})    
                return        
            }
            if(userCount>0){
                user = await db.collection("users").findOne({email:email, password : password})
                delete user.password
                console.log(user)
                res.send({status: 0, logged:true, message : "login successful" , user})      
                return      
            }else{
                res.send({status: 1, logged:false, message : "login failed"}) 
                return           
            }
        }
        catch(err){
            console.log(err)
            res.send({status: 2, logged:false, message : "login failed"})
            return            

        }

    })();
})



app.post('/request-chat', function (req, res) {
    let user1 = req.body.sender
    let user2 = req.body.receiver
    const db = dbClient.db();
    db.collection("chatdatabse-sprint3")
        .find({
            $or: [{
                sender: user1,
                receiver: user2
            }, {
                sender: user2,
                receiver: user1
            }]
        })
        .sort({
            timestamp: -1
        })
        .limit(100)
        .toArray((err, results) => {
            if (err) {
                console.log(err);
                return
            }
            res.json({
                code: 200,
                chats: results
            })
        });
});


app.post('/new-message', function (req, res) {

    const db = dbClient.db();
    let data = {
        "sender": req.body.sender,
        "receiver": req.body.receiver,
        "message": req.body.msg
    }
    db.collection("chatdatabse-sprint3").insertOne(data).then(res => {
        res.json({
            code: 200,
            message: 'message sent successfully'
        })
    }).catch(err => {
        res.json({
            code: 400,
            err
        });
    })
})


app.post('/save-message', function (req, res) {
    console.log(req.body)

    let data = {
        sender: req.body.sender,
        receiver: req.body.receiver,
        message: req.body.message,
        timestamp: Date.now()
    }
    if (data.message == null) {
        data.message = '';
    }
    const db = dbClient.db();
    db.collection("chatdatabse-sprint3").insertOne(data).then(results => {
        console.log(results);
        res.json({
            "code": 200,
            "msg": "inserted"
        })
    }).catch(err => {
        console.log(err);
        res.json({
            "code": 500,
            "msg": "not inserted"
        })
    });
})

app.post('/chatbot', function (req, res) {
    let msg = req.body.message

    try{
        axios.get("https://cca-duppalav1-pravar1.azurewebsites.net/microservice1?movieName=" + msg).then((results)=>{
            res.json({
                code: 200,
                chats: "Title : " + results.Title + "| Plot : " + results.Plot
            })
        })
    }
    catch(err){
        res.json({
            code:200,
            chats: "Didn't find that, try again!"
        })
    }

    
});